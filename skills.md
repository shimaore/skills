Skills

# About / à propos

Level / Niveau = knowledge / savoir, connaissance:

- 0.0 ~= I've heard about it but I haven't practiced / j'en ai entendu parler
- 0.2 ~= entry-level / noob
- 0.5 ~= mastery / maîtrise
- 0.7 ~= daily usage / utilisation quotidienne
- 0.9 ~= I've trained about it / formateur/expert
- 1.0 ~= I wrote it, I'm the reference on it / j'ai écrit le code

This is a logarithmic scale. Hours on topic ~= `10^(4*level)`:
- 0.0 ~= 1h
- 0.2 ~= 7h (~one day)
- 0.5 ~= 100h (~two weeks full-time)
- 0.7 ~= 650h (~four months full-time)
- 0.9 ~= 4000h (~two years full-time)
- 1.0 ~= 10000h (~"expert")

Désire:
- -1 ~= absolutely not / pas du tout
- -0.5 ~= I'd rather not / plutôt pas
- 0.0 ~= neutral / neutre
- 0.5 ~= OK, sure!
- 1.0 ~= that's where I want to be / c'est là que je veux être.

# Prepare

## Write commercial proposals

0.5/0.5

# Plan

# Design - Architect / Concevoir

## Embedded Systems / Systèmes embarqués

0.30 / 0.90

### Read and interpret electronics schematics / Lire et interprêter des schémas électroniques

0.30 / 0.50

### Read and interpret electronics components datasheets / Lire et interprêter des spécifications de composants électroniques

0.20 / 0.50

### Implement a simple electronics design

Conception d'un projet à base d'Atmel AVR pour Open Bidouille Camp 2014

0.55 / 0.45

#### Design electronics layout

0.3/0.5

#### Select and procure electronic components

0.3/0.5

#### Soldering

0.3/0.5

#### Platforms

##### Raspberry Pi

0.3/0.2

##### ESP32

Hardware interface for FlightGear (OSS flight simulator: cockpit hardware) & Linux Midi (live performance), in-progress.

0.25 / 0.65

#### Protocols
##### USB

0.25/0.2

##### SPI

0.25/0.2

## OpenSource VoIP systems

0.92 / 0.91

### FreeSwitch

0.90 / 0.90

### OpenSIPS

0.91 / 0.92

### SIP

0.90 / 0.90

### WebRTC

0.60 / 0.90

### wireshark & pcap

SIP and SDP protocol-level analysis

## ClosedSource VoIP systems

### Cisco CallManager

0.50 / -0.10

## Event Streaming

### RabbitMQ

0.70 / 0.40

### Axon (Node.js)

Axon follows the concepts introduced by ZeroMQ, with a native implementation in Node.js.

0.90 / 0.40

### NATS

0.10 / 0.80

### Socket.io

0.60/0.80

## Data Streaming, Big Data

0.40/0.90

### CRDT

Implemented real-time CRDTs. Project: blue-rings.

0.43 / 0.80

## Web Services: API (REST)

0.70 / 0.80

## DNS

Designed and implemented a CouchDB-backed dynamic DNS server for SIP domains management. Project: willing-toothbrush

0.40/0.30

## Network

### Routing and switching design
### IPv4 and IPv6 address assignment


# Develop: Write code / Écrire du code

## Lexer and Parser
### Bison, Jison

0.65 / 0.82

### Flex

0.65 / 0.81

## Programming Languages / Langages de programmation

### JavaScript/ECMAScript

A dynamically typed language, inspired by Scheme.

0.80 / 1.00

### CoffeeScript

0.90/1.00

### C

0.70 / 0.60

### C++

0.70 / 0.40

### Rust

0.00 / 0.20

### Assembly

CPU-level!

#### Atmel AVR

0.70 / 0.10

#### WebAssembly

### Erlang

0.20 / 0.60

### Scheme

0.10/0.20

### SmallTalk

0.10/0.10

### Perl

0.50/0.00

### COBOL

0.10/0.00

### JAVA

0.10 / -0.50

### Bash (Unix shell)

0.50/0.80

### sed
### awk
### grep, egrep
### rsync

### Forth

0.31 / 0.20

## Platforms

### Linux desktop

### Linux server

0.9 / 1.0

### Nodes.js

Libuv meets V8.

0.60 / 1.00

### Atmel AVR

0.70 / 0.10

### webassembly

0.04/0.20

### Navigator

#### Surplus

Functional reactive web based on S.js

0.57/0.23

### Electron

0.24/0.20

### Qt

0.10/0.00

### Gtk

0.10/0.00

## Machine & Deep Learning

0.10 / 1.00

## Databases, Datastores

### CouchDB

0.80 / 1.00

### PostgreSQL

0.45 / 0.30

### MySQL

0.40 / -0.10

### Redis

0.60 / 0.50

### MongoDB

0.5 / 0.2

## GIS

### GRASS GIS

0.55 / 0.00

### QGis

0.60 / 0.20

## GNSS

### GNSS interfacing

Writing code to interface with NS-HP high-precision GNSS

0.50 / 0.78

## 3D design

### OpenSCAD
### Wings3D

### Blender
### PoVRay


# Deploy - Implement / Déployer

## DevOps

### Gitlab/CI

0.60 / 0.90

## Hardware deployment

### Electrical Wiring

0.50 / 0.10

### Rack servers, switches, routers, …

0.60 / 0.10


# Operate

## HTTP

- LetsEncrypt
- Lighttpd

## SMTP, IMAP, POP

0.7 / 0.0

## DNS
### Bind

0.7/0.0

## Virtual Machines and Containers

### Docker

0.60 / 0.80

### Proxmox

0.40 / -0.10 Using the Proxmox API, designed and integrated automated virtual machines containing Docker containers deployment, integrated with Gitlab CI. Project: mamooth-door

### Automate Virtual Machines (VM) build

0.40 / 0.40 Automated the creation and deployment of VMs based on AlpineLinux. Project: mammoth-door

## Monitor

### Prometheus

0.40 / 0.75

### Grafana

0.45 / 0.75

## Network

### Wireshark

0.6 / 0.5

### Layer 2 switching

0.5 / 0.5

### Layer 3 switching
### Static routing
#### Linux

0.5/0.5

#### Cisco

0.5/0.5

### OSPF

0.5/0.0

#### Cisco
### BGP
#### Cisco
### Access-lists (ACL)
#### Cisco
#### Linux iptables/iptables2
### NAT
#### Linux iptables/iptables2

Network Configuration


## Electrical

- Read and interpret an electrical or electromechanical schematics
- Select and procure electrical components
- Electrical wiring: residential
- Electrical wiring: light industrial (water station)

## Plumbing

- Water plumbing: residential
- Water plumbing: light industrial (water station and underground distribution network)

## Sound technics

### Select and procure sound equipment

### Prepare and deploy sound systems for live musical production

- Animation de soirées
- live musical group: Poulpies

## 3D Printing

## Others (probably obsolete)

### Batch processing on VAX/VMS

# Optimize

# Cultural / Culturel

## Geopolitics
## Adapt to a new culture
(worked in Mayotte and rural Philippines)
## Languages
### English
#### written English
0.90/1.00
#### spoken English
0.80/1.00
### French
#### written French
0.90/1.00
#### spoken French
0.85/1.00
### German
### Shimaore
### Bisaya
## Clown
## Massages

# Teach, train / Former

## CouchDB

- BrestJS
- Private course

0.4/0.8

## TCP/IP

- Code.bzh (CCNA-level classes) — 2019
- CESI — 2020-2021

0.4/0.9

## Linux

- CESI — 2020-2021

0.4/0.9

# Leadership

## Management of a circus school

- Dédale de Clown (Brest, FR)

## Management of a shared space

- IMC (Urbana, IL)
- Le 214 (Brest)

0.3/0.0

# Sports
## Voile
## Aviron
0.30/1.00
